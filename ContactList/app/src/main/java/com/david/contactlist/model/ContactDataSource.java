package com.david.contactlist.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by david on 04/07/15.
 */
public class ContactDataSource {
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public ContactDataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public int insertContact(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NAME, contact.getName());
        values.put(DBHelper.COLUMN_PHONE, contact.getPhone());
        values.put(DBHelper.COLUMN_INTERESTS, contact.getInterests());
        values.put(DBHelper.COLUMN_MARITAL_STATUS, contact.getMarital_status());
        values.put(DBHelper.COLUMN_SEX, contact.getSex());

        long insertId = database.insert(DBHelper.TABLE_CONTACTS, null, values);


        database.close();

        return (int) insertId;
    }

    public void deleteContact(int contact_Id) {
        database.delete(DBHelper.TABLE_CONTACTS, DBHelper.COLUMN_ID + "= ?",
                new String[]{String.valueOf(contact_Id)});

        database.close();
    }


    public void updateContact(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NAME, contact.getName());
        values.put(DBHelper.COLUMN_PHONE, contact.getPhone());
        values.put(DBHelper.COLUMN_INTERESTS, contact.getInterests());
        values.put(DBHelper.COLUMN_MARITAL_STATUS, contact.getMarital_status());
        values.put(DBHelper.COLUMN_SEX, contact.getSex());


        database.update(DBHelper.TABLE_CONTACTS, values, DBHelper.COLUMN_ID + "= ?",
                new String[]{String.valueOf(contact.getContact_ID())});
        database.close();
    }


    public ArrayList<Contact> getContactList() {
        String selectQuery =  "SELECT  " +
                DBHelper.COLUMN_ID + "," +
                DBHelper.COLUMN_NAME + "," +
                DBHelper.COLUMN_PHONE + "," +
                DBHelper.COLUMN_INTERESTS + "," +
                DBHelper.COLUMN_MARITAL_STATUS + "," +
                DBHelper.COLUMN_SEX +
                " FROM " + DBHelper.TABLE_CONTACTS;


        ArrayList<Contact> contactArrayList = new ArrayList<Contact>();

        Cursor cursor = database.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                Contact contact = cursorToContact(cursor);
                contactArrayList.add(contact);

            } while (cursor.moveToNext());
        }

        cursor.close();

        return contactArrayList;

    }


    public Contact getContactById(int Id){

        String selectQuery =  "SELECT  " +
                DBHelper.COLUMN_ID + "," +
                DBHelper.COLUMN_NAME + "," +
                DBHelper.COLUMN_PHONE + "," +
                DBHelper.COLUMN_INTERESTS + "," +
                DBHelper.COLUMN_MARITAL_STATUS +
                DBHelper.COLUMN_SEX +
                " FROM " + DBHelper.TABLE_CONTACTS +
                " WHERE " + DBHelper.COLUMN_ID + "=?";

        int iCount =0;
        Contact contact = new Contact();

        Cursor cursor = database.rawQuery(selectQuery, new String[] { String.valueOf(Id) } );

        if (cursor.moveToFirst()) {
            do {
                contact = cursorToContact(cursor);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return contact;
    }



    private Contact cursorToContact(Cursor cursor) {
        Contact contact = new Contact();
        contact.setContact_ID(cursor.getInt(0));
        contact.setName(cursor.getString(1));
        contact.setPhone(cursor.getString(2));
        contact.setInterests(cursor.getString(3));
        contact.setMarital_status(cursor.getInt(4) > 0);
        contact.setSex(cursor.getInt(5) > 0);
        return contact;
    }

}
