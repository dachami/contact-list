package com.david.contactlist.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.david.contactlist.R;
import com.david.contactlist.model.Contact;
import com.david.contactlist.model.ContactDataSource;

import java.sql.SQLException;

public class AddContactActivity extends Activity {
    private EditText editTextName, editTextPhone, editTextInterests;
    private Spinner spinnerMaritalStatus, spinnerSex;
    private Contact contact;
    private ContactDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextInterests = (EditText) findViewById(R.id.editTextInterests);
        spinnerMaritalStatus = (Spinner) findViewById(R.id.spinnerMaritalStatus);
        spinnerSex = (Spinner) findViewById(R.id.spinnerSex);

        ActionBar actionBar = getActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        datasource = new ContactDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void addContact(View view) {
        String name = editTextName.getText().toString().trim();
        String phone = editTextPhone.getText().toString().trim();
        String interests = editTextInterests.getText().toString().trim();
        Boolean maritalStatus = spinnerMaritalStatus.getSelectedItem().toString().equals("Soltero(a)")?true:false;
        Boolean sex = spinnerSex.getSelectedItem().toString().equals("Casado(a)")?true:false;

        if (!name.isEmpty() && !phone.isEmpty() && !interests.isEmpty()){
            contact = new Contact(name,phone,interests,maritalStatus,sex);
            if (datasource.insertContact(contact)!=-1){
                Toast.makeText(this, "Contacto agregado.", Toast.LENGTH_SHORT).show();
                clearForm();
            }else{
                Toast.makeText(this, "Error al agregar el contacto.", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Favor de completar los campos.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

    public void clearForm(){
        editTextName.setText("");
        editTextInterests.setText("");
        editTextPhone.setText("");
        spinnerMaritalStatus.setSelection(0);
        spinnerSex.setSelection(0);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
