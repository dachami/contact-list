package com.david.contactlist.model;

/**
 * Created by david on 04/07/15.
 */
public class Contact implements Comparable<Contact>{
    private String name, phone, interests;
    private boolean marital_status, sex;
    private int contact_ID;

    public Contact() {
    }

    public Contact(String name, String phone, String interests, boolean marital_status, boolean sex) {
        this.name = name;
        this.phone = phone;
        this.interests = interests;
        this.marital_status = marital_status;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public boolean getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(boolean marital_status) {
        this.marital_status = marital_status;
    }

    public boolean getSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public int getContact_ID() {
        return contact_ID;
    }

    public void setContact_ID(int contact_ID) {
        this.contact_ID = contact_ID;
    }

    @Override
    public int compareTo(Contact another) {
        return getName().compareTo(another.getName());
    }
}
