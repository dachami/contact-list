package com.david.contactlist.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by david on 04/07/15.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "contacts.db";

    public static final String TABLE_CONTACTS = "contacts";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_INTERESTS = "interests";
    public static final String COLUMN_MARITAL_STATUS = "marital_status";
    public static final String COLUMN_SEX = "sex";


    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_CONTACTS = "CREATE TABLE " + TABLE_CONTACTS  + "("
                + COLUMN_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + COLUMN_NAME + " TEXT, "
                + COLUMN_INTERESTS + " TEXT, "
                + COLUMN_PHONE + " TEXT, "
                + COLUMN_MARITAL_STATUS + " BOOLEAN, "
                + COLUMN_SEX + " BOOLEAN )";


        db.execSQL(CREATE_TABLE_CONTACTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
