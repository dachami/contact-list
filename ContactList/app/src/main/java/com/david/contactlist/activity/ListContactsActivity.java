package com.david.contactlist.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.david.contactlist.R;
import com.david.contactlist.adapter.AdapterContact;
import com.david.contactlist.model.Contact;
import com.david.contactlist.model.ContactDataSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class ListContactsActivity extends Activity {
    private ListView listViewContacts;
    private AdapterContact adapterContact;
    private ArrayList<Contact> contactArrayList;
    private ContactDataSource datasource;
    private AlertDialog dialogContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contacts);
        listViewContacts = (ListView) findViewById(R.id.listViewContacts);

        ActionBar actionBar = getActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        datasource = new ContactDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        contactArrayList = datasource.getContactList();

        Collections.sort(contactArrayList);

        Log.i("TAG", "Tam: " + contactArrayList.size());


        adapterContact = new AdapterContact(this, contactArrayList);

        listViewContacts.setAdapter(adapterContact);
        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("TAG", "Pos: " + position);
                displayAlertDialog(position);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void displayAlertDialog(int position) {
        Contact contact = contactArrayList.get(position);

        LayoutInflater inflater = this.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.layout_contact_info, null);
        TextView textViewName = (TextView) alertLayout.findViewById(R.id.textViewName);
        TextView textViewPhone = (TextView) alertLayout.findViewById(R.id.textViewPhone);
        TextView textViewMaritalStatus = (TextView)
                alertLayout.findViewById(R.id.textViewMaritalStatus);
        TextView textViewSex = (TextView) alertLayout.findViewById(R.id.textViewSex);
        TextView textViewInterests = (TextView) alertLayout.findViewById(R.id.textViewInterests);

        textViewName.setText(contact.getName());
        textViewPhone.setText(contact.getPhone().toString());
        textViewMaritalStatus.setText(contact.getMarital_status() == true ? "Soltero(a)" : "Casado(a)");
        textViewSex.setText(contact.getSex() == true ? "Masculino" : "Femenino");
        textViewInterests.setText(contact.getInterests());

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("CONTACTO");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });



        dialogContact = alert.create();
        dialogContact.show();


    }
}