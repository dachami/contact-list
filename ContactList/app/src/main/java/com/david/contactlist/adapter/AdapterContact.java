package com.david.contactlist.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.david.contactlist.R;
import com.david.contactlist.model.Contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

/**
 * Created by david on 04/07/15.
 */
public class AdapterContact extends ArrayAdapter<Contact> implements DialogInterface.OnClickListener, SectionIndexer {
    private ArrayList<Contact> contactArrayList;
    private Activity context;
    private HashMap<String, Integer> alphaIndexer;
    private String[] sections;
    final Random mRandom = new Random(System.currentTimeMillis());


    public AdapterContact(Activity context, ArrayList<Contact> objects) {
        super(context, R.layout.layout_row_contact);
        this.context = context;
        this.contactArrayList = objects;

        alphaIndexer = new HashMap<String, Integer>();
        int cont=0;
        for (Contact contact: objects) {
            String s = contact.getName().substring(0, 1).toUpperCase();
            if (!alphaIndexer.containsKey(s))
                alphaIndexer.put(s, cont);
            cont++;
        }

        Set<String> sectionLetters = alphaIndexer.keySet();
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
        Collections.sort(sectionList);
        sections = new String[sectionList.size()];
        for (int i = 0; i < sectionList.size(); i++)
            sections[i] = sectionList.get(i);


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.layout_row_contact, null);

        TextView textViewName = (TextView) item.findViewById(R.id.textViewName);
        TextView textViewPhone = (TextView) item.findViewById(R.id.textViewPhone);
        TextView textViewFirstLetter = (TextView) item.findViewById(R.id.textViewFirstLetter);

        textViewFirstLetter.setBackgroundColor(generateRandomColor());
        textViewFirstLetter.setText(String.valueOf(contactArrayList.get(position).getName().charAt(0)));
        textViewName.setText(contactArrayList.get(position).getName());
        textViewPhone.setText(contactArrayList.get(position).getPhone().toString());
        return item;

    }

    @Override
    public int getCount() {
        return contactArrayList.size();
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return alphaIndexer.get(sections[sectionIndex]);
    }

    @Override
    public int getSectionForPosition(int position) {
        for ( int i = sections.length - 1; i >= 0; i-- ) {
            if ( position >= alphaIndexer.get( sections[ i ] ) ) {
                return i;
            }
        }
        return 0;
    }

    public int generateRandomColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }
}
