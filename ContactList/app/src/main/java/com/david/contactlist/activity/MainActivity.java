package com.david.contactlist.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.david.contactlist.R;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void list_contacts(View view) {
        Intent i = new Intent(MainActivity.this,ListContactsActivity.class);
        startActivity(i);
    }

    public void add_contacts(View view) {
        Intent i = new Intent(MainActivity.this,AddContactActivity.class);
        startActivity(i);
    }


}
